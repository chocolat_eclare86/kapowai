module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
      server: {
        options: {},
      }
    },
    
    sass: {
      dist: {
        options: {
          style: 'compressed'
        },
        files: {
          'css/build-styles.css': 'css/build-syles.scss',
        }
      }
    },
    jshint: {
      files: ['js/*.js'],
    },
    watch: {
      options: {
        livereload: true,
      },
      html: {
        files: ['index.php'],
      },
      js: {
        files: ['js/**/*.js'],
        tasks: ['jshint'],
      },
      sass: {
        options: {
          // Monitor Sass files for changes and compile them, but don't reload the browser.
          livereload: true,
        },
        files: ['scss/**/*.scss'],
        tasks: ['sass'],
      },
      css: {
        // LiveReload on the CSS files instead of their Sass source files and you get
        // the style to refresh without reloading the page in the browser.
        files: ['css/**/*.css'],
      },
    },
  });

  // Actually running things.
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-notify');

  // Default task(s).
  grunt.registerTask('default', ['connect', 'watch']);

};

module.exports = function(grunt) {

  //project configurations
  grunt.initConfig({

      cssmin : {
          target : {
              src : ["css/build-styles.css", "css/style2.css"],
              dest : "dist/build-styles.min.css"
          }
      }

  });

  //load cssmin plugin
  grunt.loadNpmTasks('grunt-contrib-cssmin');

  //create default task
  grunt.registerTask("default", ["cssmin"]);

};
