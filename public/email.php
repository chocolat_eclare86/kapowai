<!-- PHP FORM SENDING -->
<?php
  require '../private/phpmailer/PHPMailerAutoload.php';
  include_once('../private/phpmailer/class.phpmailer.php');
  require_once('../private/phpmailer/class.smtp.php');

  $formmessage = '';
  $formstatus = '';

    if (isset($_POST['submit'])) {
      $mail = new PHPMailer;

      $to = 'hello@claremansfield.co.nz';
      $firstname = $_POST['firstname'];
      $lastname = $_POST['lastname'];
      $email = $_POST['email'];
      $phone = $_POST['phone'];
      $message = $_POST['message'];
      $secretKey = "6LcU7FIUAAAAAA3A0kxbE1ryio2uaXZPUpjzVKOr";
      $responseKey = $_POST['g-recaptcha-response'];
      $userIP = $_SERVER['REMOTE_ADDR'];

      $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
      $response = file_get_contents($url);
      $response = json_decode($response);
      if ($response->success) {



        $mail->isSMTP();
        $mail->Host = 'sg2plcpnl0213.prod.sin2.secureserver.net';

        $mail->SMTPSecure = 'tls';
        $mail->Port = 587;



        $mail->isHTML(true);
        $mail->Body = '<p>Hi Clare</p> <p><strong>New Website Contact Message</strong></p><p>Name :' . $firstname . $lastname . '</p><p>Email :' . $email . '</p><p>Phone Number :' . $phone . '</p><p>Message :' . $message . '</p>';
        $mail ->AltBody = $firstname . $lastname . $email . $phone . $message;

        //Set who the message is to be sent to
        $mail->addAddress('hello@claremansfield.co.nz');
        //Set the subject line
        $mail->Subject = "WEBSITE LEAD";
        // //Read an HTML message body from an external file, convert referenced images to embedded,
        // //convert HTML into a basic plain-text alternative body
        // $mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
        //Replace the plain text body with one created manually

        //send the message, check for errors
        if (!$mail->send()) {
          $formstatus = 'error';
          $formmessage = "OOPS! Something went horribly wrong ...: " . $mail->ErrorInfo;
        } else {
          $formstatus = 'success';
            $formmessage = " Thanks $firstname, Your message was sent successfully. I will be in touch shortly";
        }

      } else {
        $formstatus = 'error';
        $formmessage = "Oops, did you forget to check the ReCaptcha?";
      }
    }
?>
