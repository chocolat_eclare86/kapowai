<?php
  include_once('email.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr" id="homepage">
    <head>
        <?php include 'modules/meta.php';?>
        <title>KIA | Kapowai Immigration Advice</title>

        <!-- TO DO -->
        <!-- favicons -->
        <?php // include 'modules/favicons.php';//?>
        <!-- opengraph -->
        <?php //include 'modules/opengraph.php';//?>
        <!-- Google analytics -->
        <?php //include 'modules/analytics.php';//?>

        <!-- link to compiled & minified css -->
        <link rel='stylesheet' type='text/css' href='dist/build-styles.min.css'>

        <!-- javascript links-->
        <script src='http://code.jquery.com/jquery-1.11.1.min.js' async></script>

        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" ></script>
        <![endif]-->

    </head>
    <body class="top">
        <!-- Error Message -->
        <div class="error">
            <div id="<?php echo $formstatus; ?>" >
                <?php echo $formmessage; ?>
            </div>
        </div>

        <div class="container">
            <div class="content-wrap">
                <h1>Hello World</h1>
                <p>Im testing out some stuff I have never done before</p>

                <a href="/dashboard/admin.php">Admin</a>
            </div><!-- /content-wrap -->
        </div><!-- /container -->
    </body>
</html>
