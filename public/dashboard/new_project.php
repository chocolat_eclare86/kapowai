<?php
session_start();

if (isset($_SESSION['username'])) {

include_once('database-link.php');
include_once('../private/brandingProjectConfig.php');

$sql = "SELECT * FROM $bpTable1";

$result = mysqli_query($link, $sql) or die (mysqli_error($link));

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <body>
<!-- This section has not been modified yet to fit this project. This will serve as an interface to add data to a specified mysql database-->

        <h1>Add New Branding Project to Database</h1>

        <form action="insert.php" method="POST" enctype="multipart/form-data">

        Project Name: <input type="text" name="project_name"></br>
        Project Uri <small>Please only use lowercase, and NO spaces</small>: <input type="text" name="project_uri"></br>
        Logo Path: <input type="file" name="project_logo"></br>
        Description: <br>
            <textarea name="project_details" rows="10" cols="30"></textarea></br>
        Image Path: <input type="file" name="project_image"></br>

        <button type="submit" value="Submit" name="submit">Submit</button>

        </form>

    </body>
</html>
<?php

} else {
  ?>
  <p>Ooops you are not logged in!</p>
  <a href="admin.php" alt="login">Admin</a>
  <?php
}
?>
