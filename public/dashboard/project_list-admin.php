<?php
session_start();

if (isset($_SESSION['username'])) { // if you are logged in show page

    include_once('database-link.php');
    include_once('../private/brandingProjectConfig.php');

    $sql = "SELECT * FROM $bpTable1";

    $result = mysqli_query($link, $sql) or die (mysqli_error($link));

    // if(isset($_POST['delete'])) { //if you click delete
    //
    //     $id = $_POST['id'];
    //
    //     $sqlDel = "DELETE FROM $bpTable1 WHERE id=$id";
    //
    //     if ($link->query($sqlDel) === TRUE) {
    //         echo "Record deleted successfully";
    //     } else {
    //         echo "Error deleting record: " . $link->error;
    //     }
    if(isset($_GET["del"])){
        $id = $_GET["del"];
        if($link->query("DELETE FROM $bpTable1 WHERE id=$id")){
            ?>
            <script type="text/javascript">
                if (confirm("Are you sure you want to delete this record?")) {
                    header('Location: project_list-admin.php');
                } else {
                    txt = "Record not deleted";
                }
            </script>
        <?php
        } else {
            echo "Failed to delete record.";
        }
    }



    echo "<form method='POST'>";
    echo "<table border='1'>";
    echo "<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>URI</th>
    <th>LOGO</th>
    <th>CONTENT</th>
    <th>IMAGE</th>
    <th>Admin Controls</th>
    </tr>";

    while($row = mysqli_fetch_array($result)) {
        $id = $row['id'];
        $name = $row['projectName'];
        $uri = $row['projectUri'];
        $logo = $row['projectLogo'];
        $content = $row['projectContent'];
        $image = $row['projectImage'];
        echo "
        <tr>
        <td>".$id."</td>
        <td>".$name."</td>
        <td>".$uri."</td>
        <td>".$logo."</td>
        <td>".$content."</td>
        <td>".$image."</td>
        <td>
            <a class='button alert delete' href='?del=".$row["id"]."'>Delete</a>
            <a class='button edit' href='update.php?edit=".$row["id"]."'>Edit</a>
        </td>
        </tr>";
    }



    echo "</table>
    </form>";

    mysqli_close($link);

    } else { // if you are not logged in
        ?>
        <div class="alert alert-error" role="alert">
           <p>Sorry. You need to be logged in to view this page.</p>
        </div>

        <a href="admin.php" alt="login">Admin</a></br></br>
        <a href="index.php" alt="Home">Home</a>
        <?php
    }

?>
