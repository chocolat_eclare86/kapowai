<!DOCTYPE html>
<html lang="en" dir="ltr" id="homepage">
    <head>
        <?php include 'site-variables.php';?>
        <title>DASHBOARD | <?php echo $siteName ?></title>
        <!-- future link to favicons module -->
        <!-- future link to opengraph module -->
        <!-- future link to Google analytics module -->

        <!-- Ajax -->
        <?php include 'dash-page-display.php'; ?>

        <!-- future link to compiled & minified css -->
        <!-- <link rel='stylesheet' type='text/css' href='../dist/build-styles.min.css'> -->

        <!-- temporary link to compiled (un-minified css) -->
        <link rel='stylesheet' type='text/css' href='../css/build-styles.css'>

        <!-- javascript links-->
        <script src='http://code.jquery.com/jquery-1.11.1.min.js' async></script>

        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js" ></script>
        <![endif]-->

    </head>

    <body id="dashboard">
        <?php session_start();

        if (isset($_SESSION['username'])) { ?>
            <div class="dash-sidebar">
                <?php include 'dashboard-sidebar.php'; ?>
            </div>
            <?php
        }
        else {
            ?>

            <div class="alert alert-error" role="alert">
                <p>Sorry. You need to be logged in to view this page.</p>
            </div>

            <a href="admin.php" alt="login">Admin</a></br></br>
            <a href="index.php" alt="Home">Home</a>
            <?php
        }
        if(isset($_SESSION['username'])){
        ?>
        <div class="dash-display" id="dash-display">
            <!-- ajax display page in here -->
            <h1>Welcome</h1>
            <p>This content should be replaced when you click the css test link to your left. I'm also playing around with some Ajax.</p>
        </div>
        <?php
        }
        ?>
    </body>
</html>
