<!-- Again - this page has not been modified to suit the project as of yet. This page right now displays all the data from a specified datatable on a mysql database.-->
<?php
include_once('database-link.php');
include_once('../private/brandingProjectConfig.php');

$sql = "SELECT * FROM $bpTable1";

$result = mysqli_query($link, $sql) or die (mysqli_error($link));

echo "<table border='1'>";
echo "<tr>
<th>NAME</th>
<th>URI</th>
<th>LOGO</th>
<th>CONTENT</th>
<th>IMAGE</th>
</tr>";

while($row = mysqli_fetch_array($result)) {
    $name = $row['projectName'];
    $uri = $row['projectUri'];
    $logo = $row['projectLogo'];
    $content = $row['projectContent'];
    $image = $row['projectImage'];
    echo "<tr><td style='width: 300px;'>".$name."</td><td>".$uri."</td><td><img src='".$logo."' alt='".$name."'></td><td>".$content."</td><td><img src='".$image."' alt='".$name."'></td></tr>";
}

echo "</table>";
mysqli_close($link);
?>
