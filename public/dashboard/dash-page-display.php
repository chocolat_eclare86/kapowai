<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {
    $('.sidebar-link').click(function() {
        //Get the id of the clicked link and assign it to a variable
        var ID = $(this).attr('id');
        // append .php to the variable to get the file name
        ID += '.php';
        // display the file in the dash-display div
        $("#dash-display").load(ID);
    });
});

</script>
