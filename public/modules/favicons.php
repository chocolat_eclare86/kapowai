<!-- TO DO -->

<link rel="apple-touch-icon" sizes="180x180" href="images/meta/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/meta/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/meta/favicon-16x16.png">
<link rel="manifest" href="images/meta/site.webmanifest">
<link rel="mask-icon" href="images/meta/safari-pinned-tab.svg" color="#3e3ef9">
<meta name="msapplication-TileColor" content="#3e3ef9">
<meta name="theme-color" content="#1b1464">
